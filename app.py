import requests
import time
import json
import datetime

def apiCall(t):
    #url variable for calling the api
    url = "http://stream.meetup.com/2/rsvps"
    #API call
    r = requests.get(url, stream=True)
    li = []
    #setting our end time
    t_end = time.time() + t
    #iterating through each object from the API
    for line in r.iter_lines():
        if line:
            #format to JSON
            decoded_line = line.decode('utf-8')
            li.append(json.loads(decoded_line))
        #check if time is up!
        if time.time() > t_end:
            break #time is up, break the loop
    return li

def getFurthestDate(li):
    #date of the event furthest into the future
    time_stamp = 0
    url = None
    #comparing the dates from epoch
    for i in range(len(li)):
        try:
            if li[i]['event']['time'] > time_stamp:
                try:
                    time_stamp = li[i]['event']['time']
                except:
                    continue #event time not included...
                url = li[i]['event']['event_url'] #add the url as well
        except:
            continue
    date = time.strftime('%Y-%m-%d %H:%M', time.localtime(time_stamp / 1000))
    return date, url

#this function maps countries to occurences from the rsvp app
def topCountries(li):
    d = {}
    for i in range(len(li)):
        try:
            if li[i]['group']['group_country'] not in d:
                d[li[i]['group']['group_country']] = 1
            else:
                d[li[i]['group']['group_country']] += 1
        except:
            continue
    return d

#main function invocation
if __name__ == '__main__':
    print("Welcome to Please Respond.\n")
    #getting configurable api listening time
    t = input("How many seconds would you like to collect data? ")
    try:
        t = int(t)
    except:
        t = 60
        print("Incorrect input. Defaulting to 60 seconds\n")
    print("Listening to API...\n")
    current = time.time()
    li = apiCall(t)
    ending_time = time.time() - current
    ending_time = round(ending_time, 1)
    print(f'API connection closed. Elapsed time: {ending_time} sec\n')

    #total number of rsvp events detected
    amount_rsvp = len(li)
    #get the furthest date event and the url associated with it
    date, url = getFurthestDate(li)
    #top three countries receiving the most RSVP events
    d = topCountries(li)
    #processing the data from topCountries
    ans = []
    #sort the dict
    for item in sorted(d, key=d.get):
        ans.append(item)
    #depending on our answer, we will print output to console
    if len(ans) >= 3:
        x, y, z = ans.pop(), ans.pop(), ans.pop()
        print(f'{amount_rsvp},{date},{url},{x},{d[x]},{y},{d[y]},{z},{d[z]}')
    elif len(ans) == 2:
        x, y = ans.pop(), ans.pop()
        print(f'{amount_rsvp},{date},{url},{x},{d[x]},{y},{d[y]}')
    else:
        x = ans.pop()
        print(f'{amount_rsvp},{date},{url},{x},{d[x]}')